import dynamic_traffic_assignment as dta
import time
#network = dta.network_from_csv('scenarios/00_topology.csv')
#configuration = dta.json2dictionary('scenarios/00_sim_configuration.json')
network =dta.network_from_csv('scenarios/simulations/validation/validation_2/topology.csv')
configuration = dta.json2dictionary('scenarios/simulations/validation/validation_2/sim_configuration.json')
sim = dta.Simulation(config=configuration,network=network)
start = time.perf_counter()
sim.run()
stop = time.perf_counter()
print(stop-start)