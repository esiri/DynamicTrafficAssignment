class MissingSimultationParameter(Exception):
    """
    Rise an exception when one mandatory simulation parameters is missing.
    """
    def __init__(self):

        self.message =  'In simulation config file one or more simulation parameters are missing!'
        super().__init__(self.message)

class ParameterNotAllowed(Exception):
    """
    Rise an exception when one simulation parameters has a proibitted value
    """

    def __init__(self, wrong_parameter, message = 'Simulation parameter not valid!'):

        self.wrong_parameter = wrong_parameter
        self.message = message
        super().__init__(self,self.message)

class UnkownFundamentalDiagram(Exception):
    """
    Rise an exception when an unknown fundamental diagram is used. 
    """

    def __init__(self,wrong_fund_diagram, message = 'Unkown Fundamental Diagram!'):
        
        self.wrong_fund_diagram = wrong_fund_diagram
        self.message = message
        super().__init__(self,self.message)

class MissingFundamentalDiagramParameter(Exception):
    """
    Rise an exception when whitin the edge combined fd parameters set
    one or more parameters are missing.
    """
    def __init__(self,missing_fd_parameter, message = 'Missing Fundamental Diagram Parameter'):
        self.missing_parameter = missing_fd_parameter
        self.message = message
        super().__init__(self.self.message)

class AttributeSettingError(Exception):
    """
    Attribute (constant) already set! Cannot be modified again!
    """
    
    def __init__(self,attribute,message = 'Attribute is a Constant, cannot be modifies again!'):
        
        self.attribute = attribute
        self.message = message
        super().__init__(self,self.message)

class UnkownVehicleType(Exception):
    """
    Simple Vehicle istances are not allowed. 
    A vehicle_obj.__class__ must be Vehicle subclass (e.g. Car, Truck,... )
    """
    def __init__(self,attribute=None,message='A vehicle obj.__class__ must be Vehicle subclass (e.g. Car,Truck,...)'):
        
        self.attribute = attribute
        self.message = message
        super().__init__(self,self.message)

class InconsistentData(Exception):
    """
    Raise an error when data shows some inconsistences.
    """
    def __init__(self,attribute=None, message='Data inconsistency detected!'):
        self.attribute = attribute
        self.message = message
        super().__init__(self,self.message)