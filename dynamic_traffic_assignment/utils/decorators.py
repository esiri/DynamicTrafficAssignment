import functools


def check_all_required_parameters(required_parameters: set):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kargs):
            func(*args, **kargs)

        return wrapper

    return decorator