import json
import networkx as nx
import pandas as pd
from dataclasses import dataclass
    


def network_from_csv(network_csv):
    """
    Executes the pre-processing to create the network objects (from csv) required for the 
    sim initialization process.

    Input: path to a .csv file containing an edge list (delimiter = ";")
    Output: Network type object < MultiDiGraph

    csv file structure
        - key: edge unique key
        - source: edge source node
        - target node: edge target node
        - other attributes
    """

    edge_list_dataframe = pd.read_csv(network_csv, delimiter=';')
    exclude_columns = {'key', 'source', 'target'}
    edge_parameters = list(set(edge_list_dataframe.columns) - exclude_columns)

    G = nx.from_pandas_edgelist(edge_list_dataframe, source='source',
                                target='target',
                                edge_key='key',
                                create_using=nx.MultiDiGraph,
                                edge_attr=[column for column in edge_list_dataframe.columns if
                                           column not in 'source target key'.split()])

    return G

def json2dictionary(config_path):
    """
    Convert a json data structure into a python dictionary.

    Input:
        - config.json file path
    Output:
        - config dictionary obj
    """

    config_str = open(config_path)
    config = json.load(config_str)
    return config