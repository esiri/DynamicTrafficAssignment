import numpy as np
from dynamic_traffic_assignment.utils.utils import show_obj_attributes
import dynamic_traffic_assignment.utils.custom_exceptions as custom_exceptions

#NOTE Agatha approach --> each class has it own set of fd coefficients, regardless of the road segment carachteristics

class Vehicle:
    """
    Vehicle type specific fundamental diagram modifier coefficients.
    """

    __ALLOWED_FD_SHAPE = ['triangular', 'greenshields']
    FD_SHAPE = None


    def __init__(self):
        self.show = show_obj_attributes.__get__(self, self.__class__)

    def max_dQ(self):
        """
        Compute max derivative of Q with respect of t
        """
        if self.type:
            try:
                max_dQ = max(self.v_max,self.w)
            except:
                max_dQ = self.v_max
                # --> v_max souhld always be present, regardles of the fundamental diagram 
            return max_dQ
        else:
            raise custom_exceptions.UnkownVehicleType()


    def show_attrs(self):
        """
        Show vehicle info in one shot
        """

        # Vehicle info
        vehicle_type = f'type: {self.type}'

        if self.FD_SHAPE == 'triangular':
            vehicle_fundamental_diagram = f'fundamental diagram: triangular'
            vehicle_v_max = f'v_max: {self.v_max}'
            vehicle_w = f'w: {self.w}'
            vehicle_rho_max = f'rho max: {self.rho_max}'
            vehicle_rho_cr = f'rho critical: {self.rho_cr}'
            vehicle_q_max = f'q max: {self.q_max}'

            # Output string
            return  f'Vehicle info:\n{vehicle_fundamental_diagram}\n{vehicle_type}\n{vehicle_v_max}\n{vehicle_w}\n{vehicle_rho_max}\n{vehicle_rho_cr}\n{vehicle_q_max}'

            
            
        elif self.FD_SHAPE == 'greenshields':
            vehicle_fundamental_diagram = f'fundamental diagram: greenshields'
            vehicle_v_max = f'v_max: {self.v_max}'
            vehicle_rho_max = f'rho max: {self.rho_max}'
            vehicle_rho_cr = f'rho critical: {self.rho_cr}'
            vehicle_q_max = f'q max: {self.q_max}'

            # Output string
            return  f'Vehicle info:\n{vehicle_fundamental_diagram}\n{vehicle_type}\n{vehicle_v_max}\n{vehicle_rho_max}\n{vehicle_rho_cr}\n{vehicle_q_max}'



class Car(Vehicle):
    """
    Car vehicle class.
    """
    
    def __init__(self):
        super().__init__()

        self.type = 'car'

        if self.FD_SHAPE == 'triangular':
            self.v_max = 100
            self.w = 30
            self.rho_max = 200 
            self.rho_cr = self.w * self.rho_max / (self.w + self.v_max) 
            self.q_max = self.v_max * self.rho_cr

        # greenshields FD
        elif self.FD_SHAPE == 'greenshields':
            self.v_max = 100
            self.rho_max = 80
            self.rho_cr = self.rho_max * 0.5
            self.q_max = self.rho_cr * self.v_max        


class Truck(Vehicle):
    """"
    Truck vehicle class.
    """

    def __init__(self):
        super().__init__()

        self.type = 'truck'

        # triangular FD
        if self.FD_SHAPE == 'triangular':
            self.v_max = 80
            self.w = 30
            self.rho_max = 78
            self.rho_cr = self.w * self.rho_max / (self.w + self.v_max) 
            self.q_max = self.v_max * self.rho_cr
        
        # greenshields FD
        elif self.FD_SHAPE == 'greenshields':
            self.v_max = 80
            self.rho_max = 60
            self.rho_cr = self.rho_max * 0.5
            self.q_max = self.rho_cr * self.v_max

class Bike_Agatha(Vehicle):
    """
    Bike vehicle class from Agatha work.
    """
    
    def __init__(self):
        super().__init__()

        self.type = 'bike_agatha'

        if self.FD_SHAPE == 'triangular':
            raise custom_exceptions.UnkownFundamentalDiagram(message='Agatha simulation uses Greenshields Fundamental Diagram')

        # greenshields FD
        elif self.FD_SHAPE == 'greenshields':
            self.v_max = 20 #km/h
            self.rho_max = 500 #veh/km
            self.rho_cr = self.rho_max * 0.5
            self.q_max = self.rho_cr * self.v_max

class Car_Agatha(Vehicle):
    """
    Car vehicle class from Agatha work.
    """

    def __init__(self):
        super().__init__()
        self.type = 'car_agatha'

        if self.FD_SHAPE == 'triangular':
            raise custom_exceptions.UnkownFundamentalDiagram(message='Agatha simulation uses Greenshields Fundamental Diagram')

        elif self.FD_SHAPE == 'greenshields':
            self.v_max = 70 #km/h
            self.rho_max = 150 #veh/km
            self.rho_cr = self.rho_max * 0.5
            self.q_max = self.rho_cr * self.v_max
