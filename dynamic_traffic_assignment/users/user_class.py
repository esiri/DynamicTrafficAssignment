from dynamic_traffic_assignment.utils.utils import show_obj_attributes


class User:
    """
    Class for users. A user class is uniquely identified by
        - an origin node
        - a destination node
        - a strategy
        - class-specific fundamental diagram

    For implementation convenience each user class has the following properties:
        - id (must be unique --> correspond to the relative "slice" within the "core tensor structure"
        - origin node
        - destination node
        - strategy (a dictionary of distribution matrices) {node: numpy_matrix}
        - behaviour (a function that generates a strategy depending on certain factors) [function(*) --> strategy]
        - veh_type (influence the class-specific fundamental diagram)
    """

    def __init__(self,**attrs):
        
        for key,value in attrs.items():
            setattr(self,key,value)

        self.show = show_obj_attributes.__get__(self, self.__class__)


    def show_attrs(self):
        """
        Show all relevant attributes in one shot. 
        """
        # Base info
        user_id = f'user id: {self.idx}'
        user_origin = f'origin: {self.origin}'
        user_destination = f'destination: {self.destination}'
        user_demand = f'demand: {self.demand}'
        user_supply = f'supply: {self.supply}'

        base_info_string = f'User info:\n{user_id}\n{user_origin}\n{user_destination}\n{user_demand}\n{user_supply}'
        
        # Vehicle info
        user_vehicle = f'{self.vehicle.show_attrs()}'

        # Behaviour info
        user_behaviour = f'{self.behaviour.show_attrs()}'

        print(f'{base_info_string}\n\n{user_vehicle}\n\n{user_behaviour}')

        




