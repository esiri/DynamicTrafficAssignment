import numpy as np
import sympy
import warnings
from sympy.utilities.autowrap import ufuncify
import dynamic_traffic_assignment.utils.custom_exceptions as custom_exceptions

class FundamentalDiagram():
    """
    Traffic fundamental diagram class.
    """

    def __init__(self, num_parameters, parameters):
        
        self.num_parameters = num_parameters
        self.parameters = parameters

    def combine_coefficients(self, edge_obj,users):
        """
        Returns a series of coefficient, suited to be used for edge related operations,
        consistent with the fundamental diagram as a result of some sort of "combination"
        between edge_param and class_param

        edge_param:
            * v_max = ndarray.size --> [num_cells]
            * ...
        class_param:
            * v_max = ndarray.size --> scalar
        """
        # Simple combination
        for coefficient in self.parameters: # <-- set changes depending on the type of FundamentalDigram!
            # class specific fd coefficients
            class_coefficients = np.array([getattr(user.vehicle, coefficient) for _,user in users.items()])

            # no edge specific fd coefficients

            # combination rule
            combined_coefficient = getattr(edge_obj,coefficient) #<-- aliasing
            combined_coefficient[:,:] = np.array([class_coefficients,]*edge_obj.num_cells).transpose()

    def show_diagram(self):
        """
        "Pretty print" of the fundamental diagram
        """

        print('V(rho):')
        sympy.pprint(self.V)
        
        print('\nQ(rho):')
        sympy.pprint(self.Q)

    def show_vehicle_specific_diagram(self,vehicle):
        """
        "Pretty print" vehicle specific fundamental diagram
        """
        rho = sympy.Symbol('rho')
        V = self.V.copy()
        Q = self.Q.copy()
        for parameter in self.parameters:
            parameter_value = getattr(vehicle, parameter)
            V = V.subs(parameter, parameter_value)
            Q = Q.subs(parameter, parameter_value)

        print(f'{vehicle.type.capitalize()} V(rho):')
        sympy.pprint(V)
        sympy.plotting.plot(V,(rho, 0, vehicle.rho_max))
        
        print(f'\n{vehicle.type.capitalize()} Q(rho):')
        sympy.pprint(Q)
        sympy.plotting.plot(Q,(rho, 0, vehicle.rho_max))


class Triangular(FundamentalDiagram):
    """
    Triangular type fundamental diagram subclass.
    """

    def __init__(self,fun_diagr_vectorization = 'python1'):

        num_parameters = 5
        set_parameters = {
            'v_max',
            'q_max',
            'rho_cr',
            'rho_max',
            'w'
        }
        super().__init__(num_parameters=num_parameters,parameters=set_parameters)

        fun_diagram_selector = {
            'python1': self.__vect_diagrams,
            'python2': self.__vect_diagrams2,
            'ufunc': self.__ufuncify_diagram
        }

        # FUNDAMENTAL DIAGRAM ..................................................................
        v_max, w, rho_cr, rho_max, q_max, rho = sympy.symbols('v_max, w, rho_cr, rho_max, q_max, rho')
        
        # Velocity
        self.V = sympy.Piecewise((0,rho < 0),(v_max, rho <= rho_cr), (w * (rho_cr / rho - 1) +  q_max / rho, rho<rho_max),(0,True))

        # Flow
        self.Q = (rho * self.V).simplify()

        # Vectorize diagrams
        self.__vector_V, self.__vector_Q = fun_diagram_selector[fun_diagr_vectorization]()

    def __vect_diagrams(self):

        def vect_V(v_max,w,rho_cr,rho_max,q_max,rho):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning) 
                result = np.where((0<=rho)&(rho<=rho_cr), v_max ,\
                    np.where((rho>rho_cr)&(rho<=rho_max), w * (rho_cr / rho - 1) +  q_max / rho,0))
            return result 

        def vect_Q(v_max,w,rho_cr,rho_max,q_max,rho):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)
                result = np.where((0<=rho)&(rho<=rho_cr), rho * v_max ,\
                    np.where((rho>rho_cr)&(rho<=rho_max), w * (rho_cr - rho) +  q_max,0))
            return result
        return vect_V, vect_Q

    def __vect_diagrams2(self):

        def vect_V(v_max,w,rho_cr,rho_max,q_max,rho):
            return np.select([(0<=rho)&(rho<=rho_cr),(rho>rho_cr)&(rho<=rho_max)],[v_max,w * (rho_cr / rho - 1) +  q_max / rho],0)

        def vect_Q(v_max,w,rho_cr,rho_max,q_max,rho):
            return np.select([(0<=rho)&(rho<=rho_cr),(rho>rho_cr)&(rho<=rho_max)],[rho * v_max,w * (rho_cr - rho) +  q_max],0)

        return vect_V,vect_Q

    def __ufuncify_diagram(self):
        """
        Invoke sympy.utils.autowrap.unfuncify to generate a binary function that supports broadcasting on numpy arrays.
        Modifier can be used to target a specific language ['C' or 'F95'(a.k.a fortran)]. Otherwise the tragetted language is
        choosen based on the backend (numpy by default).

        reference: https://docs.sympy.org/latest/modules/utilities/autowrap.html
        """

        v_max, w, rho_cr, rho_max, q_max, rho = sympy.symbols('v_max, w, rho_cr, rho_max, q_max, rho')

        V = ufuncify([v_max,w,rho_cr,rho_max,q_max,rho], self.V)
        Q = ufuncify([v_max,w,rho_cr,rho_max,q_max,rho], self.Q)

        return V,Q

    def get_V(self,edge,time_step):
        """
        Trigger __ufunc_V to compute the speed, given a rho vector, for each class on each cell of an edge.
        """
        # all this vectors have same dimension
        r = edge.total_rho[:,time_step]
        v_max = edge.v_max
        q_max = edge.q_max
        rho_cr = edge.rho_cr
        rho_max = edge.rho_max
        w = edge.w

        return  self.__vector_V(v_max,w,rho_cr,rho_max,q_max,r)

    def get_Q(self,edge,time_step):
        """
        Trigger __ufunc_Q to compute the flow, given a rho vector, for each class on each cell of an edge.
        """

        # all this vectors have same dimension
        rho = edge.rho[:,:,time_step]
        rho = np.zeros([rho.shape[0],rho.shape[1]])
        rho = rho + 150
        v_max = edge.v_max
        q_max = edge.q_max
        rho_cr = edge.rho_cr
        rho_max = edge.rho_max
        w = edge.w

        return self.__vector_Q(v_max,w,rho_cr,rho_max,q_max,rho)

    def max_dQ(self,edge):
        """
        Compute the norm of the maximum derivative.
        """
        try:
            dQ = np.max([edge.v_max, edge.w])
            return dQ
        except:
            message = f'combined v_max or w are missing on edge {edge}' 
            raise custom_exceptions.MissingFundamentalDiagramParameter(missing_fd_parameter='v_max or w', message=message)
        
    def get_edge_demands_supplies(self,edge,time_step):
        """
        Compute demand and supply for all edge's cells
        """
        r = edge.total_rho[:,time_step]
        v_max = edge.v_max
        q_max = edge.q_max
        rho_cr = edge.rho_cr
        rho_max = edge.rho_max
        w = edge.w

        r_min = np.minimum(r,rho_cr)
        r_max = np.maximum(r,rho_cr)

        demand = self.__vector_Q(v_max,w,rho_cr,rho_max,q_max,r_min)
        supply = self.__vector_Q(v_max,w,rho_cr,rho_max,q_max,r_max)

        return demand,supply

class Greenshields(FundamentalDiagram):
    """
    Greenshields type fundamental diagram subclass.
    """

    def __init__(self, fun_diagr_vectorization = 'python'):
        num_parameters = 2
        set_parameters = {
            'v_max',
            'rho_max'
        }

        super().__init__(num_parameters=num_parameters,parameters=set_parameters)

        fun_diagram_selector = {
            'python': self.__vect_diagrams,
            'ufunc': self.__ufuncify_diagram
        }

        #FUNDAMENTAL DIAGRAM ........................................
        v_max, rho_max, rho = sympy.symbols('v_max, rho_max, rho')

        # Velocity
        self.V = sympy.Piecewise((0,rho < 0),(v_max * (1-rho/rho_max),rho<=rho_max), (0,True))

        # Flow
        self.Q = (rho * self.V).simplify()

        # Vectorize diagrams
        self.__vector_V, self.__vector_Q = fun_diagram_selector[fun_diagr_vectorization]()


    def __vect_diagrams(self):

        def vect_V(v_max,rho_max,rho):
            return np.where((0<rho)&(rho<=rho_max),v_max * (1-rho/rho_max),0)
            #return np.select([(0<=rho)&(rho<=rho_max)],[v_max * (1-rho/rho_max)],0)
        def vect_Q(v_max,rho_max,rho):
            return np.where((0<rho)&(rho<=rho_max),rho * v_max * (1-rho/rho_max),0)
            #return np.select([(0<=rho)&(rho<=rho_max)],[rho*v_max * (1-rho/rho_max)],0)

        return vect_V, vect_Q

    def __ufuncify_diagram(self):
        """
        Invoke sympy.utils.autowrap.unfuncify to generate a binary function that supports broadcasting on numpy arrays.
        Modifier can be used to target a specific language ['C' or 'F95'(a.k.a fortran)]. Otherwise the targetted language is
        choosen based on the backend (numpy by default).

        reference: https://docs.sympy.org/latest/modules/utilities/autowrap.html
        """

        v_max, rho_max, rho = sympy.symbols('v_max, rho_max rho')

        ufunc_V = ufuncify([v_max, rho_max, rho], self.V)
        ufunc_Q = ufuncify([v_max, rho_max, rho], self.Q)   

        return ufunc_V, ufunc_Q  

    
    def get_V(self,edge,time_step):
        """
        Trigger __ufunc_V to compute the speed, given a rho vector, for each class on each cell of an edge.
        """

        r = edge.total_rho[:,time_step]
        v_max = edge.v_max
        rho_max = edge.rho_max

        return self.__vector_V(v_max,rho_max,r)

    def get_Q(self,edge,time_step):
        """Trigger __ufunc_Q to compute the speed, given a rho vector, for each class on each cell of an edge.
        """

        r = edge.total_rho[:,time_step]
        v_max = edge.v_max
        rho_max = edge.rho_max

        return self.__vector_Q(v_max,rho_max,r)
    
    def max_dQ(self,edge):
        """
        Compute the norm of the maximum derivative.
        """
        try:
            dQ = np.max(edge.v_max)
            return dQ
        except:
            message = f'combined v_max id missing on edge {edge}' 
            raise custom_exceptions.MissingFundamentalDiagramParameter(missing_fd_parameter='v_max', message=message)
    
    def get_edge_demands_supplies(self,edge,time_step):
        """
        Compute demand and supply for all edge's cells.
        """
        r = edge.total_rho[:,time_step]
        v_max = edge.v_max
        rho_max = edge.rho_max
        rho_cr = edge.rho_max / 2

        r_min = np.minimum(r, rho_cr)
        r_max = np.maximum(r,rho_cr)

        demand = self.__vector_Q(v_max,rho_max,r_min)
        supply = self.__vector_Q(v_max,rho_max,r_max)

        return demand, supply